package com.read.d;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import jxl.Sheet;
import jxl.Workbook;


public class Passingmultipledatausingjxl {
	
	public static void main(String[] args) throws Throwable {
		
		WebDriver driver=new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
		
		
		driver.get("https://demowebshop.tricentis.com/");
		File f=new File("C:\\Users\\Suriya prasath j\\eclipse-workspace\\DataDriven\\details.xlsx");
		FileInputStream fis = new FileInputStream(f);
		Workbook wb=Workbook.getWorkbook(fis);
		
		Sheet sh=wb.getSheet("register");
		
		int rows=sh.getRows();
		int columns=sh.getColumns();
		
		for(int i=1;i<rows;i++) {
			String username=sh.getCell(0, i).getContents();
			String password=sh.getCell(1, i).getContents();
			
			driver.findElement(By.xpath("//a[contains(text(),'Log in')]")).click();
			driver.findElement(By.id("Email")).sendKeys(username);
			driver.findElement(By.id("Password")).sendKeys(password);
			Thread.sleep(10000);
			driver.findElement(By.xpath("//input[@value='Log in']")).click();
			
			driver.findElement(By.xpath("//a[contains(text(),'Log out')]")).click();
			
		}
		
	}

}
